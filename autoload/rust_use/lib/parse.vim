
" Character types:
let s:ALPHANUM      = 1
let s:COLON         = 2
let s:SEMICOLON     = 4
let s:OPEN_BRACKET  = 8
let s:CLOSE_BRACKET = 16
let s:COMMA         = 32
let s:STAR          = 64
let s:SPACE         = 16384
let s:OTHER         = 32768

" Some character masks:
let s:ALL           = 65535
let s:ALL_BUT_SPACE = s:ALL - s:SPACE
let s:BRACKETS      = s:OPEN_BRACKET + s:CLOSE_BRACKET
let s:WORD_CHARS    = s:SEMICOLON + s:BRACKETS + s:COMMA + s:STAR

" Determine if a path part is a single token (like 'std', 'collections') or
" a group (like '{HashMap, HashSet}'):
let s:PATH_PART_SINGLE = 1
let s:PATH_PART_GROUP  = 2


"'''''''''''''''''''' function! rust_use#lib#parse#part_is_group(part)
function! rust_use#lib#parse#part_is_group(part)
    return a:part.path_part_type == s:PATH_PART_GROUP
endf

"'''''''''''''''''''' function! rust_use#lib#parse#part_is_single(part)
function! rust_use#lib#parse#part_is_single(part)
    return a:part.path_part_type == s:PATH_PART_SINGLE
endf

"'''''''''''''''''''' function! rust_use#lib#parse#parse_use_statement(start_line_nr)
function! rust_use#lib#parse#parse_use_statement(start_line_nr)
    let start_line = getline(a:start_line_nr)
    let found = match(start_line, '^use \+\zs')
    if found == -1
        throw 'Not a use statement'
    endif
    let pos = { 'l': a:start_line_nr, 'c': found+1 }
    return rust_use#lib#parse#parse_path(pos)
endf

"'''''''''''''''''''' function! rust_use#lib#parse#parse_path(from_pos)
function! rust_use#lib#parse#parse_path(from_pos)
    call rust_use#lib#debug#debug_pos(a:from_pos, "Parse PATH,")

    let pos = a:from_pos
    let path_parts = []
    let index = 0
    let path = {
    \   'start': a:from_pos,
    \   'parts': path_parts,
    \}

    while 1
        call rust_use#lib#debug#inc_indent()

        let path_part = s:parse_path_part(pos)
        call extend(path_part, {
        \   'parent_path': path,
        \   'parent_path_index': index,
        \})

        call rust_use#lib#debug#debug_range_line(path_part, "path_part:") | call rust_use#lib#debug#dec_indent()

        call add(path_parts, path_part)

        let next_token = s:get_next_word(path_part.end)
        if next_token.word != '::'
            break
        endif
        let pos = s:get_next_word(next_token.end).start
        let index += 1
    endw

    let path.end = path_parts[-1].end
    return path
endf

"'''''''''''''''''''' function! s:parse_path_part(from_pos)
function! s:parse_path_part(from_pos)
    call rust_use#lib#debug#debug_pos(a:from_pos, "Parse PART,")

    let word = s:get_word(a:from_pos)

    if word.type == s:ALPHANUM || word.type == s:STAR
        return {
        \   'start'         : word.start,
        \   'end'           : word.end,
        \   'path_part_type': s:PATH_PART_SINGLE,
        \   'word'          : word.word,
        \}
    elseif word.type == s:OPEN_BRACKET
        let group_start = word.start

        " Build the 'children' variable:
        let children = []
        call rust_use#lib#debug#show("GROUP:") | call rust_use#lib#debug#inc_indent()
        while 1
            " Get the next non-comma token:
            while 1
                let word = s:get_next_word(word.start)
                " TODO: comma parsing is too tolerant:
                if word.type != s:COMMA | break | endif
            endw

            if word.type == s:CLOSE_BRACKET
                break
            endif

            " Parse the child path:
            let path_part = rust_use#lib#parse#parse_path(word.start)

            call add(children, path_part)
            let word = s:get_word(path_part.end) " Prepare for the next path

            call rust_use#lib#debug#debug_range_line(path_part, "path:")
            call rust_use#lib#debug#debug_object(word)
        endw
        call rust_use#lib#debug#dec_indent()

        let end = word.end " TODO: will crash with empty brackets

        return {
        \   'start'         : group_start,
        \   'end'           : end,
        \   'path_part_type': s:PATH_PART_GROUP,
        \   'children'      : children,
        \}
    else
        throw printf("Unexpected token: '%s' line %s column |%s|", word.word, word.start.l, word.start.c)
    endif
endf

"'''''''''''''''''''' function! s:get_word(from_pos)
" This function assumes that the given position is the beginning of the wanted
" word ( = non-space character, valid character position)
function! s:get_word(from_pos)
    let line = a:from_pos.l
    let line_str = getline(line)
    let col = a:from_pos.c - 1
    let char_type = s:get_char_type(line_str[col])
    let start = { 'l': a:from_pos.l, 'c': a:from_pos.c, 'type': char_type }
    let end = s:get_word_end(start)
    let word = strpart(line_str, start.c-1, end.c-start.c+1)

    return {
    \   'start': start,
    \   'end': end,
    \   'word': word,
    \   'type': start.type,
    \}
endf

"'''''''''''''''''''' function! s:get_next_word(from_pos)
function! s:get_next_word(from_pos)
    let start = s:get_next_word_start(a:from_pos)
    let line_str = getline(start.l)
    let end = s:get_word_end(start)
    let word = strpart(line_str, start.c-1, end.c-start.c+1)

    return {
    \   'start': start,
    \   'end': end,
    \   'word': word,
    \   'type': start.type,
    \}
endf

"'''''''''''''''''''' function! s:get_next_word_start(from_pos)
function! s:get_next_word_start(from_pos)
    let line = a:from_pos.l
    let line_str = getline(line)
    let col = a:from_pos.c - 1
    let last_line = line('$')

    " Determine the char type to search (the one which will mark a new wor(l)d):
    let first_char_type = s:get_char_type(line_str[col])
    let type_to_search = s:ALL_BUT_SPACE
    if first_char_type != s:SPACE && !and(first_char_type, s:WORD_CHARS)
        let type_to_search -= first_char_type
    endif

    " Loop over each char in the buffer, starting from the position given as a function argument:
    while 1
        let col += 1
        " Go to the next line if reached the end of line:
        if col >= strlen(line_str)
            while 1
                let line += 1
                if line > last_line
                    throw "No more word, reached end of file"
                endif
                let line_str = getline(line)
                if !empty(line_str)
                    break
                endif
            endw
            let col = 0
        endif

        let char = line_str[col]
        let char_type = s:get_char_type(char)

        if and(char_type, type_to_search)
            return {
            \   'l': line,
            \   'c': col+1,
            \   'type': char_type,
            \}
        elseif char_type == s:SPACE
            let type_to_search = s:ALL_BUT_SPACE
        endif
    endw
endf

"'''''''''''''''''''' function! s:get_word_end(from_pos)
function! s:get_word_end(from_pos)
    let line = a:from_pos.l
    let line_str = getline(line)
    let col = a:from_pos.c - 1

    " Determine the char type to search (the one which will mark a new wor(l)d):
    let first_char_type = s:get_char_type(line_str[col])

    if and(first_char_type, s:WORD_CHARS)
        return { 'l': a:from_pos.l, 'c': a:from_pos.c, 'type': first_char_type }
    endif

    " Loop over each char in the buffer, starting from the position given as a function argument:
    while 1
        let col += 1
        if col >= strlen(line_str)
            break
        endif
        let char = line_str[col]
        if s:get_char_type(char) != first_char_type
            break
        endif
    endw

    return { 'l': line, 'c': col+1-1, 'type': first_char_type }
endf

"'''''''''''''''''''' function! s:get_char_type(char)
function! s:get_char_type(char)
    if a:char == "\t" || a:char == " "           | return s:SPACE
    elseif match(a:char, '^[A-Za-z0-9_]$') != -1 | return s:ALPHANUM
    elseif a:char == '{'                         | return s:OPEN_BRACKET
    elseif a:char == '}'                         | return s:CLOSE_BRACKET
    elseif a:char == ':'                         | return s:COLON
    elseif a:char == ':'                         | return s:SEMICOLON
    elseif a:char == ','                         | return s:COMMA
    elseif a:char == '*'                         | return s:STAR
    else                                         | return s:OTHER
    endif
endf
