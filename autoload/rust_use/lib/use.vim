
" FINDING USES:

"'''''''''''''''''''' function! rust_use#lib#use#find_used_item(item, ...)
function! rust_use#lib#use#find_used_item(item, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()
    for use in all_uses
        let paths = s:get_detailed_paths(use)
        for path in paths
            if path[-1].word == a:item
                return path
            endif
        endfor
    endfor
    return {}
endf

"'''''''''''''''''''' function! rust_use#lib#use#check_item_defined(item, ...)
" Check if the given item is not already defined in a use statement.
function! rust_use#lib#use#check_item_defined(item, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()
    let path = rust_use#lib#use#find_used_item(a:item, all_uses)
    if !empty(path)
        let line_nr = path[-1].start.l
        throw printf("The item '%s' is already defined line %i", a:item, line_nr)
    endif
endf

"'''''''''''''''''''' function! rust_use#lib#use#find_all_uses()
function! rust_use#lib#use#find_all_uses()
    let uses = []
    let line_nrs = s:find_all_use_lines()
    for line_nr in line_nrs
        call add(uses, rust_use#lib#parse#parse_use_statement(line_nr))
    endfor

    return uses
endf

"'''''''''''''''''''' function! s:find_all_use_lines()
function! s:find_all_use_lines()
    let line_nrs = []
    let lines = getline(1, '$')
    for line_nr in range(1, len(lines))
        let line = lines[line_nr-1]
        if match(line, '^use ') != -1
            call add(line_nrs, line_nr)
        endif
    endfor

    return line_nrs
endf

"'''''''''''''''''''' function! s:find_places_to_group_path(path, ...)
function! s:find_places_to_group_path(path, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()
    let path_len = len(a:path)
    let places = []
    let longest_len = 0

    let minimum_common_path = a:path[0] == 'std' ? g:rust_use__minimum_common_path : 0

    for use in all_uses
        let detailed_paths = s:get_detailed_paths(use)
        let use_place = {}
        let longest_use_len = 0

        " Find the best path in this use, where the new path could be grouped:
        for detailed_path in detailed_paths
            let common_len = 0
            let min_length = min([ path_len, len(detailed_path) ])
            for i in range(min_length)
                if a:path[i] != detailed_path[i].word
                    break
                endif
                let common_len += 1
            endfor
            " Update the best candidate only if the common length is longer than the older
            " candidate:
            if common_len > longest_use_len && common_len >= minimum_common_path
                let use_place = {
                \   'use': use,
                \   'detailed_path': detailed_path,
                \   'path': s:get_path_from_detailed_path(detailed_path),
                \   'common_len': common_len,
                \}
                let longest_use_len = common_len
            endif
        endfor

        if empty(use_place)
            continue
        endif

        " Keep only the longest places (common length):
        if longest_use_len > longest_len
            let places = []
            let longest_len = longest_use_len
        endif
        if longest_use_len == longest_len
            call add(places, use_place)
        endif
    endfor

    return places
endf


" GENERATE NEW USE STATEMENTS:

"'''''''''''''''''''' function! rust_use#lib#use#find_last_use_line(...)
function! rust_use#lib#use#find_last_use_line(...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()
    if empty(all_uses)
        return 0
    endif
    return all_uses[-1].end.l
endf

"'''''''''''''''''''' function! rust_use#lib#use#create_use_statement(path, ...)
function! rust_use#lib#use#create_use_statement(path, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()
    let statement = printf('use %s;', rust_use#lib#path#join(a:path))
    call append(rust_use#lib#use#find_last_use_line(all_uses), statement)
endf


" EDIT USE STATEMENTS:

"'''''''''''''''''''' function! rust_use#lib#use#add_path(path, ...)
function! rust_use#lib#use#add_path(path, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()

    " Check if the item is not already defined in a use statement:
    call rust_use#lib#use#check_item_defined(a:path[-1])

    " Find the right place where to add the path:
    let places = s:find_places_to_group_path(a:path, all_uses)
    let force_creation = 0
    if g:rust_use__ask_placement && len(places) > 1
        let place = rust_use#lib#interactive#ask_placement(places)
        if empty(place) | return | endif
    elseif !empty(places)
        let place = places[0]
    endif
    if empty(places) || type(place) == v:t_string && place == 'force_creation'
        call rust_use#lib#use#create_use_statement(a:path, all_uses)
        return
    endif

    " Remove the part which is common between the new path and the use group:
    let sublen = len(a:path) - place.common_len
    let final_subpath = a:path[-sublen:]

    let last_common_part = place.detailed_path[place.common_len-1]
    let siblings = s:get_part_next_siblings(last_common_part)

    if empty(siblings)
        call rust_use#lib#buffer#insert_after_pos(last_common_part.end, '::self')
        let self_pos = {'l': last_common_part.end.l, 'c': last_common_part.end.c + 3}
        let self_path = rust_use#lib#parse#parse_path(self_pos)
        let siblings = [ self_path.parts[0] ]
    endif

    call s:group_item(siblings[0], siblings[-1], final_subpath)
endf

"'''''''''''''''''''' function! s:append_to_new_group(start_part, end_part, new_path)
" Append a path to a group to be created.
" E.g., 'A::B::C' could become 'A::{B::C, My::New::Path}' if the start part is 'B' and the
" end part is 'C'.
function! s:append_to_new_group(start_part, end_part, new_path)
    let new_path_str = rust_use#lib#path#join(a:new_path)
    let range = { 'start': a:start_part.start, 'end': a:end_part.end }
    let old_subpath = rust_use#lib#buffer#get_range_str(range)

    if g:rust_use__new_group_style == 'inline'
        let format = '{%s, %s%s}'
    elseif g:rust_use__new_group_style == 'spaced-inline'
        let format = '{ %s, %s%s }'
    elseif g:rust_use__new_group_style == 'multiline'
        let line = getline(a:start_part.start.l)
        let indent1 = matchstr(line, '^\s*')
        let indent2 = indent1 . rust_use#lib#util#get_indent()
        let format = "{\n" . indent2 . "%s,\n" . indent2 . "%s%s\n" . indent1 . '}'
    endif

    let comma = g:rust_use__new_group_comma ? ',' : ''

    let replacement_str = printf(format, old_subpath, new_path_str, comma)
    call rust_use#lib#buffer#replace_range(range, replacement_str)
endf

"'''''''''''''''''''' function! s:append_to_group(group_part, new_path)
" Append to an existing group.
" E.g., '{Foo, Bar}' becomes '{Foo, Bar, My::New::Path}'
function! s:append_to_group(group_part, new_path)
    let new_path_str = join(a:new_path, '::')
    let group_str = rust_use#lib#buffer#get_range_str(a:group_part)
    let unwrapped_group_str = strpart(group_str, 1, len(group_str) -2)
    let trimmed = trim(unwrapped_group_str)

    " Detect style:
    let has_trailing_comma = match(trimmed, ',$') != -1
    let has_spaces_around = unwrapped_group_str[0] == ' '
    let is_multi_line = a:group_part.start.l != a:group_part.end.l

    " Create the modified group string:
    if !has_trailing_comma | let trimmed .= ',' | endif
    if is_multi_line
        let lines = split(group_str, "\n")
        let indent = matchstr(lines[1], '^\s*')
        let last_indent = matchstr(lines[-1], '^\s*')
        let space_after_comma = "\n" . indent
    else
        let space_after_comma = ' '
    endif
    let trimmed .= space_after_comma . new_path_str
    if has_trailing_comma | let trimmed .= ',' | endif
    if is_multi_line
        let wrapped = printf("{\n%s\n%s}", indent . trimmed, last_indent)
    else
        if has_spaces_around | let trimmed = printf(' %s ', trimmed) | endif
        let wrapped = printf('{%s}', trimmed)
    endif

    call rust_use#lib#buffer#replace_range(a:group_part, wrapped)
endf

"'''''''''''''''''''' function! s:group_item(start_part, end_part, new_path)
function! s:group_item(start_part, end_part, new_path)
    if a:start_part != a:end_part || rust_use#lib#parse#part_is_single(a:end_part)
        call s:append_to_new_group(a:start_part, a:end_part, a:new_path)
    else
        " Already a group :
        call s:append_to_group(a:end_part, a:new_path)
    endif
endf


" WORK WITH PATH AND PART OBJECTS:

"'''''''''''''''''''' function! s:get_detailed_paths(path)
function! s:get_detailed_paths(path)
    " TODO: cache the return value into a:path ?
    let head = s:get_path_head(a:path)
    let tail = a:path.parts[-1]

    if rust_use#lib#parse#part_is_single(tail)
        return [ head + [ tail ] ]
    endif

    " Tail is a group:
    let paths = []
    for child_path in tail.children
        let child_paths = s:get_detailed_paths(child_path)
        for child_path in child_paths
            call add(paths, head + child_path)
        endfor
    endfor

    return paths
endf

"'''''''''''''''''''' function! s:get_path_from_detailed_path(path)
function! s:get_path_from_detailed_path(path)
    return map(copy(a:path), {k, v -> v.word})
endf

"'''''''''''''''''''' function! s:get_paths_from_detailed_paths(paths)
function! s:get_paths_from_detailed_paths(paths)
    return map(copy(a:paths), {k, fp -> s:get_path_from_detailed_path(fp)})
endf

"'''''''''''''''''''' function! s:get_path_head(path)
function! s:get_path_head(path)
    let head = []
    for i in range(len(a:path.parts)-1)
        let part = a:path.parts[i]
        call add(head, part)
    endfor
    return head
endf

"'''''''''''''''''''' function! s:get_part_next_siblings(part)
function! s:get_part_next_siblings(part)
    let parts = a:part.parent_path.parts
    return parts[a:part.parent_path_index + 1:]
endf
