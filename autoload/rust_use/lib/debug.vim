
let s:indent = 0

"'''''''''''''''''''' function! rust_use#lib#debug#get_pos()
function! rust_use#lib#debug#get_pos()
    return { 'l': line('.'), 'c': col('.') }
endf

"'''''''''''''''''''' function! rust_use#lib#debug#show_object(obj)
function! rust_use#lib#debug#show_object(obj)
    if has_key(a:obj, 'path_part_type')
        if a:obj.path_part_type == s:PATH_PART_SINGLE | let type = 'single'
        else | let type = 'group'
        endif
        if a:obj.path_part_type == s:PATH_PART_SINGLE
            call s:show_indented(printf('Part %i (%s: %s)', a:obj.parent_path_index, type, a:obj.word))
        else
            call s:show_indented(printf('Part %i (%s)', a:obj.parent_path_index, type))
            call s:show_indented('Children: [')
            let s:indent += 1
            for child in a:obj.children
                call rust_use#lib#debug#show_object(child)
            endfor
            let s:indent -= 1
            call s:show_indented(']')
        endif
    elseif has_key(a:obj, 'parts')
        call s:show_indented('Path; parts: [')
        let s:indent += 1
        for child in a:obj.parts
            call rust_use#lib#debug#show_object(child)
        endfor
        let s:indent -= 1
        call s:show_indented(']')
    else
        call s:show_indented(printf('Word: %s', a:obj))
    endif
endf

"'''''''''''''''''''' function! rust_use#lib#debug#show_objects(objects)
function! rust_use#lib#debug#show_objects(objects)
    for obj in a:objects
        call rust_use#lib#debug#show_object(obj)
    endfor
endf

"'''''''''''''''''''' function! rust_use#lib#debug#show_path(path)
function! rust_use#lib#debug#show_path(path)
    call s:show_indented(rust_use#lib#path#join(a:path))
endf

"'''''''''''''''''''' function! rust_use#lib#debug#debug_pos(pos, ...)
function! rust_use#lib#debug#debug_pos(pos, ...)
    if !g:rust_use__debug | return | endif
    echo s:indented()
    if a:0>=1 | echon a:1 | endif
    let line = getline(a:pos.l)
    let overview = strpart(line, a:pos.c-1)
    echon printf(' Pos: #%s |%s|: "%s..."', a:pos.l, a:pos.c, overview)
endf

"'''''''''''''''''''' function! rust_use#lib#debug#show(message)
function! rust_use#lib#debug#show(message)
    if !g:rust_use__debug | return | endif
    call s:show_indented(a:message)
endf

"'''''''''''''''''''' function! rust_use#lib#debug#inc_indent()
function! rust_use#lib#debug#inc_indent()
    let s:indent += 1
endf

"'''''''''''''''''''' function! rust_use#lib#debug#dec_indent()
function! rust_use#lib#debug#dec_indent()
    let s:indent -= 1
endf

"'''''''''''''''''''' function! rust_use#lib#debug#debug_range_line(range, ...)
function! rust_use#lib#debug#debug_range_line(range, ...)
    if !g:rust_use__debug | return | endif
    echo s:indented()
    if a:0>=1 | echon a:1 | endif
    let line = getline(a:range.start.l)
    let s = strpart(line, a:range.start.c-1, a:range.end.c - a:range.start.c + 1)
    echon printf(' Range: "%s"', s)
endf

"'''''''''''''''''''' function! rust_use#lib#debug#debug_object(obj)
function! rust_use#lib#debug#debug_object(obj)
    if !g:rust_use__debug | return | endif
    call rust_use#lib#debug#show_object(a:obj)
endf

"'''''''''''''''''''' function! indented()
function! s:indented()
    return repeat('    ', s:indent)
endf

"'''''''''''''''''''' function! s:show_indented(message)
function! s:show_indented(message)
    echo s:indented() a:message
endf
