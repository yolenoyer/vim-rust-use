
"'''''''''''''''''''' function! rust_use#lib#util#show_exception()
function! rust_use#lib#util#show_exception()
	echoh ErrorMsg
	echo v:exception
	echoh None
endf

"'''''''''''''''''''' function! rust_use#lib#util#get_indent()
function! rust_use#lib#util#get_indent()
	if !&et
		return "\t"
	else
		return repeat(" ", &ts)
	endif
endf
