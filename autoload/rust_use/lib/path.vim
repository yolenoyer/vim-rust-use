
" CONVERT STRING INTO PATH LISTS:

"'''''''''''''''''''' function! rust_use#lib#path#split(s)
function! rust_use#lib#path#split(s)
    return split(a:s, '::', 1)
endf

"'''''''''''''''''''' function! rust_use#lib#path#join(path)
function! rust_use#lib#path#join(path)
    return join(a:path, '::')
endf
