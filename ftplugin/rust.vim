
if g:rust_use__enable_commands
    command! -buffer -nargs=+ -bang -complete=custom,rust_use#lib#commands#complete_use
    \   Use
    \   call rust_use#lib#commands#use('<bang>', <f-args>)

    command! -buffer -nargs=+ -bang
    \   UsePath
    \   call rust_use#lib#commands#use_path('<bang>', <f-args>)

    if g:rust_use__enable_abbr
        cabbr <buffer> u  Use
        cabbr <buffer> up UsePath
    endif
endif

if g:rust_use__enable_default_mappings
    if exists('mapleader') | let s:old_mapleader = mapleader | endif
    let mapleader = g:rust_use__leader

    imap <c-r><c-u> <plug>(rust_use-use)
    map  <leader>u  <plug>(rust_use-use)

    if exists('s:old_mapleader') | let mapleader = s:old_mapleader
    else                         | unlet mapleader | endif
endif
