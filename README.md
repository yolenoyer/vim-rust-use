# vim-rust-use

## Description

This plugin helps to quickly add rust `use` statements while editing, without the need of
moving to the top of the file.

## Features

- Smartly group `use` statements together (e.g.: if `HashMap` is needed, then
  `use std::collections::HashSet;` will be transformed
  to `use std::collections::{HashSet, HashMap};`;
- Can handle any nested groups like `use A::{B, C::{D::E, F::G}}`;
- Can add `self` if needed (e.g.: `use std::collections;` will be transformed to
  `use std::collections::{self, HashMap};` if `HashMap` is needed;
- Handle multi-line `use` statements (see below for examples);
- Adapt editing style by considering the input style, e.g., if adding `std::New` :
     * `use std::{ A, B };` => `use std::{ A, B, New };`
     * `use std::{ A, B, };` => `use std::{ A, B, New, };`
     * `use std::{A, B};` => `use std::{A, B, New};`
   * Handle (nested) multiline statements, e.g. if adding `std::B::C_New` :
     ```
        use std::{A, B::{
           C_1,
           C_2,
        }};
       ```
       =>
       ```
        use std::{A, B::{
           C_1,
           C_2,
           C_New,
        }};
       ```
- Expose several commands for rust files:
  * `:Use [id1 [id2] ... ]` : Add one or more `use` statement(s) for the given standard id(s)
  * `:UseNew [id1 [id2] ... ]` : Add one or more `use` statement(s) for the given standard id(s),
                                 but forbid grouping with existing `use` statements (force a new
                                 `use` statement creation)
  * `:UsePath [path1 [path2] ... ]` : Add a `use` statement for one (or more) custom path(s); e.g.:
                                      `:UsePath One::Two` will add the statement `use One::Two;`
                                      at the end of the `use` declarations.
- Define optional default mappings:
  * Normal mode: `<leader>u` => add the identifier under the cursor as a `use` statement (or group
    it if possible);
  * Edit mode: `<ctrl-r><ctrl-u>` => add the just-typed identifier as a `use` statement (or group
    it if possible).

## Installation

TODO

## Limitations

- Though you can add any custom `use` statement with the command `:UsePath`, the main `:Use` command
  (which allows you to add any public struct, trait, function, etc... *just by their single name*)
  will only work with the standard library.

  To be clear, you **can do** `:Use HashMap`, but for
  now you **can't do** `:Use MyExternalStruct`.

## Dependencies

None.

No rust, or even python dependencies are needed for now. This is just VimL.

## Todos

- Should be able to work with **all** identifiers defined in the current crate, including
  identifiers defined in:

   * external crates,
   * the currently used standard library, depending on the version of `rustc` and/or
     `rustup`.

  So it needs a complete rework on how identifiers are collected.

- Add an option to force the creation of a new `use` statement each time, even if a grouping is
  possible.

- Add a *path checker*: e.g., the command `:UsePath A::B!::C` should raise an error because of the
  `!` character.


## Know bugs

#### Bug #1

With the following rust source:

```
use A::B::C::D::E;
```

, executing the command `:UsePath A::B` should resolve into:

```
use A::B::{C::D::E, self};
```

But for now, here is the result:

```
use A::B::{C::D::E, A::B};
```

